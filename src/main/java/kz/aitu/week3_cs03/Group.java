package kz.aitu.week3_cs03;

public class Group {
    private String name;
    private int number;
    private Student monitor;
    private Student[] students;

    public Group() {
        students = new Student[20];
        number = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Student getMonitor() {
        return monitor;
    }

    public void setMonitor(Student monitor) {
        this.monitor = monitor;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public void addStudent(Student student) {
        students[number] = student;
        number++;
    }
}
