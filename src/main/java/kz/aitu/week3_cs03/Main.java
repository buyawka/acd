package kz.aitu.week3_cs03;

public class Main {

    public static double avgGroup(Group group) {
        int n = group.getNumber();
        double sum = 0;

        for (int i = 0; i < n; i++) {
            sum += group.getStudents()[i].getAge();
        }
        return sum / n;
    }

    public static void main(String[] args) {
        Group cs1903 = new Group();
        Group cs1904 = new Group();

        cs1903.setName("CS1903");

        Student st = new Student();
        st.setName("Azamat");
        st.setAge(17);

        cs1903.setMonitor(st);

        System.out.println(cs1903.getName());
        System.out.println(cs1903.getMonitor());

        Student st1 = new Student();
        st1.setName("Sanzhar");
        st1.setAge(19);

        Student st2 = new Student();
        st2.setName("Abilay");
        st2.setAge(5);

        Student st3 = new Student();
        st3.setName("Dias");
        st3.setAge(33);

        cs1903.addStudent(st);
        cs1903.addStudent(st1);
        cs1903.addStudent(st2);
        cs1903.addStudent(st3);

        System.out.println(avgGroup(cs1903));


    }
}
