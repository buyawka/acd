package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {

    public int findSecondMax(int max1, int max2) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        if(a == 0) return max2;

        if(a > max1) max1 = a;
        else if(a > max2) max2 = a;

        return findSecondMax(max1, max2);
    }

    public int printNumber(int n) {
        System.out.println(n + "+ 1");

        return printNumber(n-1);


    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        printNumber(a);
    }
}
