package kz.aitu.additional;

import kz.aitu.additional.array.Array;
import kz.aitu.additional.linkedList.LinkedList;

public class Main {


    public static void main(String[] args) {
        System.out.println("=====Welcome to our lessons!=====");

//        workWithArray();
        workWithLinkedList();
    }

    private static void workWithArray() {
        System.out.println("====Work with Array - Start=====");

        Array array = new Array(10);
        array.insert(0, "AITU");
        array.insert(1, "NU");
        array.insert(2, "ENU");
        array.insert(3, "IITU");
        array.insert(4, "KBTU");
        array.insert(5, "SDU");
        array.insert(8, "AHD");

        for (int i = 0; i < array.size(); i++) {
            if(array.at(i) != null) System.out.println(i + "-> " + array.at(i));
        }

        System.out.println("====Work with Array - Finish=====");
    }

    private static void workWithLinkedList() {
        System.out.println("====Work with LinkedList - Start=====");

        LinkedList linkedList = new LinkedList();
        linkedList.add("IPHONE");
        linkedList.add("SAMSUNG");
        linkedList.add("XIAOMI");
        linkedList.add("HUAWEI");
        linkedList.add("NOKIA");

        System.out.println(linkedList.size()); //5
        linkedList.add("MOTOROLLA");
        System.out.println(linkedList.size()); //6

        linkedList.addToFront("MEIZU");
        linkedList.printAll();
        //MEIZU
        //IPHONE
        //SAMSUNG
        //XIAOMI
        //HUAWEI
        //NOKIA
        //MOTOROLLA

        linkedList.removeAtEnd();
        linkedList.printAll();
        //MEIZU
        //IPHONE
        //SAMSUNG
        //XIAOMI
        //HUAWEI
        //NOKIA

        linkedList.removeAtEnd();
        linkedList.printAll();
        //MEIZU
        //IPHONE
        //SAMSUNG
        //XIAOMI
        //HUAWEI

        linkedList.removeAtFront();
        linkedList.printAll();
        //IPHONE
        //SAMSUNG
        //XIAOMI
        //HUAWEI



        linkedList.removeAtFront();
        linkedList.printAll();
        //SAMSUNG
        //XIAOMI
        //HUAWEI


        System.out.println(linkedList.size()); //3

        System.out.println("====Work with LinkedList - Finish=====");
    }
}
