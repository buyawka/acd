package kz.aitu.additional.array;

public class Array {

    private Node[] dataStorage;
    private int size;

    public Array(int size) {
        dataStorage = new Node[size];
        this.size = size;
    }

    public void insert(int i, String data) {
        Node node = new Node();
        node.setData(data);
        dataStorage[i] = node;
    }

    public int size() {
        return size;
    }

    public String at(int i) {
        if(dataStorage[i] == null) return null;
        return dataStorage[i].getData();
    }
}
