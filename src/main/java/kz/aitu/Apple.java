package kz.aitu;

public class Apple {
    String type;
    public int quality;
    public Apple sledApple;

    public Apple() {
        System.out.println("Novoe yabloko sozdano");
    }

    public void addSledApple(int q, String t) {
        Apple a = new Apple();
        a.quality = q;
        a.type = t;

        this.sledApple = a;
    }

    public void addLastApple(int q, String t) {
        Apple a = new Apple();
        a.quality = q;
        a.type = t;

        Apple current = this;
        while(current.sledApple != null) {
            current = current.sledApple;
        }
        current.sledApple = a;


    }

    public String srazuTypeiQuality() {
        return this.type + ": " + this.quality;
    }

    public Apple dayApple() {
        return this;
    }
}
