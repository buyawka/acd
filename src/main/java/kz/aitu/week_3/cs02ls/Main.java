package kz.aitu.week_3.cs02ls;

import java.util.Scanner;

public class Main {

    //task1 print linked list
    //task2 count node in linkedList
    public static void main(String[] args) {

        System.out.println("Hello to our program!");
        LinkedList list = new LinkedList();

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            System.out.print("Insert int please: (" + (i+1) + "): ");
            int a = scanner.nextInt();
            list.addBlock(new Block(a));
        }


        //print
        System.out.println("There data in our Linked List: ");
        Block b = list.getHead();
        while(b != null) {
            System.out.print(b.getValue() + " ");
            b = b.getNextBlock();
        }
        System.out.println();


        System.out.println("Size is: " + list.size());


        System.out.println("Finished.");
    }

}
