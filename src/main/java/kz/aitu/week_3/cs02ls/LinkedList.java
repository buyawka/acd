package kz.aitu.week_3.cs02ls;

public class LinkedList {

    private Block head;
    private Block tail;

    public void addBlock(Block block) {
        if (head == null) {
            head = block;
            tail = block;
        } else {
            tail.setNextBlock(block);
            tail = block;
        }
    }

    public int size() {
        int count = 0;

        Block b = head;
        while(b != null) {
            count = count + 1;
            b = b.getNextBlock();
        }

        return count;
    }

    public Block getHead() {
        return head;
    }

    public void setHead(Block head) {
        this.head = head;
    }

    public Block getTail() {
        return tail;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }
}
